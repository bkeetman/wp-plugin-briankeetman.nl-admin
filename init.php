<?php
/*
Plugin Name: BrianKeetman.nl admin
Plugin URI: http://www.briankeetman.nl
Description: Plugin to change the backend in BrianKeetman.nl style
Version: 1.0
Author: Brian Keetman
Author URI: http://www.briankeetman.nl
License: Private
*/

// The login screen

function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 {
            background-image: url(<?php echo plugins_url( 'assets/img/site-login-logo.png' , __FILE__ ) ?>);
            background-repeat: no-repeat;
            background-position:center;
            padding-bottom: 30px;
        }
        body.login div#login h1 a {
            background-image: none;
            padding-bottom: 30px;
        }
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return "http://www.briankeetman.nl";
}

add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'BrianKeetman.nl';
}

add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// Color theme admin interface

function custom_admin_color_scheme() {
    $plugin_url = get_option('siteurl') . '/wp-content/plugins/' . plugin_basename(dirname(__FILE__)) ;
    wp_admin_css_color(
	'BrianKeetman.nl',
         __('BrianKeetman.nl'),
        $plugin_url . '/assets/css/admin.css',
        array('#555555','#A0A0A0','#CCCCCC','#F1F1F1')
    );
}
add_action('admin_init','custom_admin_color_scheme');


?>
